<?php 
/**
 * @file
 * Alpha's theme implementation to display a section.
 */
?>
<div<?php print $attributes; ?>>
  <div id="admin-menu-spacer"> </div>
  <?php print $content; ?>
</div>